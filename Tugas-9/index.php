<?php

    // release 0
    require ('animal.php');
    require ('ape.php');
    require ('frog.php');
    
    $sheep = new Animal("Shaun");
    echo "Name : $sheep->name<br>";
    echo "Legs : $sheep->legs<br>";
    echo "Cold Blooded : $sheep->cold_blooded<br>";
    echo "<br>";

    $apes = new Ape("Kera Sakti");
    echo "Name : $apes->name<br>";
    echo "Legs : $apes->legs<br>";
    echo "Cold Blooded : $apes->cold_blooded<br>";
    echo "Yell : " .$apes->Yell(). "<br>";
    echo "<br>";

    $frogs = new Frog("Kera Sakti");
    echo "Name : $frogs->name<br>";
    echo "Legs : $frogs->legs<br>";
    echo "Cold Blooded : $frogs->cold_blooded<br>";
    echo "Yell : ". $frogs->Jump(). "<br>";
?>

