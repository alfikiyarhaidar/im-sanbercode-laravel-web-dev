<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <Form action="/Welcome" method="POST">
        @csrf
        <label for="first-name">First Name :</label><br>
        <input type="text" name="first_name" id="first-name"><br><br>

        <label for="last-name">Last Name :</label><br>
        <input type="text" name="last_name" id="last-name"><br><br>
        
        <label for="gender">Gender :</label><br>
        <input type="radio" name="gender" id="male" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">other</label><br><br>

        <label for="nationality">Nationality :</label><br>
        <Select id="nationality" name="nationality">
            <option value="Indonesia">Indonesian</option>
            <option value="singapore">Singapore</option>
            <option value="south africa">South Africa</option>
            <option value="nort korea">Nort Korea</option>
            <option value="japan">Japan</option>
        </Select><br><br>

        <label for="language">Language Spoken :</label><br>
        <input type="checkbox" name="language1" id="language1">
        <label for="language1">Indonesia</label><br>
        <input type="checkbox" name="language2" id="language2">
        <label for="language2">English</label><br>
        <input type="checkbox" name="language3" id="language3">
        <label for="language3">Japanese</label><br>
        <input type="checkbox" name="language4" id="language4">
        <label for="language4">Korean</label><br>
        <input type="checkbox" name="language4" id="language4">
        <label for="language5">other</label><br><br>

        <label for="bio">Bio :</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </Form>
</body>
</html>