@extends('master')

@section('pages')
    Film
@endsection


@section('konten')
<div class="row">
    <div class="col-3">
        <img src="{{$film->poster}}" alt="poster film" style="max-width:350px; width:100%">
    </div>
    <div class="col-5">
        <h2><b>"{{$film->judul}}"</b></h2>
        <h3>({{$film->tahun}})</h3>
        @forelse ($genre as $Key=>$values)
        @if ($values->id === $film->genre_id)
            <h5>Genre : {{$values->nama}}</h5>
        @endif
        @empty
            <h3>No Data</h3>
        @endforelse
        <p class="text-justify">{{$film->ringkasan}}</p>
    </div>
</div>
<br>
<a href="/film" class="btn btn-info">Kembali</a>
@endsection

@section('judul')
Detail Film
@endsection