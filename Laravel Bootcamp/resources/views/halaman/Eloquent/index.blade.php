@extends('master')

@push('script')
<script src="/template/plugins/datatables/jquery.dataTables.js"></script>
<script src="/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#film").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>  
@endpush

@section('pages')
    Film
@endsection

@section('judul')
    Daftar Film Tersedia
@endsection

@section('konten')
<a href="/film/create" class="btn btn-primary float-right">Tambah</a>
<table class="table" id="film">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Judul</th>
        <th scope="col">Ringkasan</th>
        <th scope="col">Tahun</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($film as $key=>$value)
            <tr class="text-justify">
                <td>{{$key + 1}}</th>
                <td style="width: 20%">{{$value->judul}}</td>
                <td style="width: 55%">{{$value->ringkasan}}</td>
                <td>{{$value->tahun}}</td>
                <td>
                    <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/film/{{$value->id}}/edit" class="btn btn-primary my-1">Edit</a>
                    <form action="/film/{{$value->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" align="center">No data</td>
            </tr>  
        @endforelse 
    </tbody>
  </table>
@endsection