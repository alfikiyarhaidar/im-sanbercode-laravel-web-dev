@extends('master')

@section('pages')
Film
@endsection

@section('judul')
Film Baru
@endsection

@section('konten')
<form action="/film" method="POST">
    @csrf
    <div class="form-group">
        <label for="inputJudul">Judul</label>
        <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan Judul Film ...">
        @error('judul')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputRingkasan">Ringkasan</label>
        <input type="text" class="form-control" id="ringkasan" name="ringkasan" placeholder="Tuliskan Ringkasan Film ...">
        @error('ringkasan')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputTahun">Tahun</label>
        <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tahun Rilis Film ...">
        @error('tahun')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputPoster">Poster</label>
        <input type="text" class="form-control" id="poster" name="poster" placeholder="Link Poster Film ...">
        @error('poster')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>

    <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Genre</label>
    <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="genre">
        <option value="1">Choose ...</option>
        @forelse ($genre as $key=>$values)
        <option value="{{$values->id}}">{{$values->nama}}</option>
        @empty
            <h3>Data Kosong</h3>
        @endforelse
    </select>
    <br><br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection