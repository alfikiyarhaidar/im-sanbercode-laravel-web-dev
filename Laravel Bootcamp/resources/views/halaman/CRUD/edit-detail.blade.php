@extends('master')

@section('pages')
Cast
@endsection

@section('judul')
Edit Bio Pemain Film
@endsection

@section('konten')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="inputName">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama Anda ...">
        @error('nama')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputUmur">Umur</label>
        <input type="number" class="form-control" id="umur" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur Anda ...">
        @error('umur')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputBio">Deskripsi Diri</label>
        <input type="text" class="form-control" id="bio" name="bio" value="{{$cast->bio}}" placeholder="Deskripsikan Diri Anda ...">
        @error('bio')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <a href="/cast" class="btn btn-info">Kembali</a>
    <button type="submit" class="btn btn-primary">Edit Bio</button>
</form>
@endsection