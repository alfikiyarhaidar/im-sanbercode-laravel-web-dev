@extends('master')


@section('pages')
    Cast
@endsection

@section('judul')
    Biodata Pemain
@endsection

@section('konten')
<div class="jumbotron">
    <h1 class="display-4">Hello, Everyone!</h1>
    <p class="lead">Perkenalkan, namaku {{$cast->nama}}</p>
    <hr class="my-1">
    <p>Tahun ini aku berusia {{$cast->umur}} tahun. Deskripsi diri, {{$cast->bio}}. Salam kenal semuanya ...</p>
    <a class="btn btn-info btn-lg" href="/cast" role="button">Kembali</a>
</div>
@endsection