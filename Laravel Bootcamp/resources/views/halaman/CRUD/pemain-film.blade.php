@extends('master')

@push('script')
<script src="/template/plugins/datatables/jquery.dataTables.js"></script>
<script src="/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#pemain").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>  
@endpush

@section('pages')
    Cast
@endsection

@section('judul')
    Daftar Pemain Film
@endsection

@section('konten')
<a href="/cast/create" class="btn btn-primary float-right">Tambah</a>
<table class="table" id="pemain">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary my-1">Edit</a>
                    <form action="/cast/{{$value->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" align="center">No data</td>
            </tr>  
        @endforelse 
    </tbody>
  </table>
@endsection