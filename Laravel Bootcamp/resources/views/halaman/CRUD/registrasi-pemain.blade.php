@extends('master')

@section('pages')
Cast
@endsection

@section('judul')
Form Registrasi Pemain
@endsection

@section('konten')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="inputName">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Anda ...">
        @error('nama')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputUmur">Umur</label>
        <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur Anda ...">
        @error('umur')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputBio">Deskripsi Diri</label>
        <input type="text" class="form-control" id="bio" name="bio" placeholder="Deskripsikan Diri Anda ...">
        @error('bio')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>
@endsection