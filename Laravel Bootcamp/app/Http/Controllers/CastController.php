<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // menampilkan list data para pemain film
    public function index(){
        $cast = DB::table('cast')->get();
        return view('halaman.CRUD.pemain-film',compact('cast'));
    }

    // menampilkan form untuk membuat data pemain film baru 
    public function create(){
        return view('halaman.CRUD.registrasi-pemain');
    }

    // menyimpan data baru ke tabel cast
    public function store(Request $request){
        $request -> validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
        ]);

        return redirect('/cast');
    }

    // menampilkan detail data pemain film dengan id tertentu 
    public function show($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('halaman.CRUD.detail-pemain',compact('cast'));
    }

    // menampilkan form untuk edit pemain film dengan id tertentu 
    public function edit($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('halaman.CRUD.edit-detail',compact('cast'));
    }

    // menyimpan perubahan data pemain film 
    public function update(Request $request, $id){
        $request->validate([
            'nama' =>'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id',$id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"],
            ]);
        
            return redirect('/cast');
    }

    // mengahpus data pemain film dengan id tertentu 
    public function destroy($id){
        $query = DB::table('cast')
            ->where('id',$id)
            ->delete();
        
        return redirect('/cast');
    }
}
