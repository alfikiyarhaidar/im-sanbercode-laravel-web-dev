<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    // menampilkan data-data film 
    public function index(){
        $film = Film::all();
        return view('halaman.Eloquent.index', compact('film'));
    }

    // Menambahkan data film baru 
    public function create(){
        $genre = Genre::all();
        return view('halaman.Eloquent.create', compact('genre'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'judul' => 'required',
            'tahun' => 'required',
            'ringkasan' => 'required',
            'poster' => 'required',
            'genre' => 'required',
        ]);

        Film::create([
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'ringkasan' => $request->ringkasan,
            'poster' => $request->poster, 
            'genre_id' => $request->genre,
        ]);

        return redirect('/film');
    }

    // menampilkan data berdasarkan ID 
    public function show($id){
        $film = Film::find($id);
        $genre = Genre::all();
        return view('halaman.Eloquent.show', compact('film','genre'));
    }

    // melakukan perubahan data film 
    public function edit($id){
        $genre = Genre::all();
        $film = Film::find($id);
        return view('halaman.Eloquent.edit', compact('film','genre'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'ringkasan' => 'required',
            'poster' => 'required',
            'genre' => 'required',
        ]);

        $film = Film::find($id);
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $request->poster;
        $film->genre_id = $request->genre;
        $film->update();

        return redirect('/film');
    }

    public function destroy($id){
        $film = Film::find($id);
        $film->delete();

        return redirect('/film');
    }

}
