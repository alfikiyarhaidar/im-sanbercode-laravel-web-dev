<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function GetFormView(){
        return view('form');
    }

    public function GetWelcomeView(Request $response){
        $namaDepan = $response->first_name;
        $namaBelakang = $response->last_name;

        return view('welcome_cuy', [
            'namaDepan' => $namaDepan,
            'namaBelakang' => $namaBelakang,
        ]);

    }
}
