<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', [HomeController::class, 'GetHomeView']);
Route::get('/Register', [AuthController::class, 'GetFormView']);
Route::post('/Welcome', [AuthController::class, 'GetWelcomeView']);

// Uji Coba CRUD
Route::get('/post', [PostController::class,'index']);
Route::get('/post/create',[PostController::class,'create']);
Route::post('/post', [PostController::class,'store']);
Route::get('/post/{post_id}',[PostController::class,'show']);
Route::get('/post/{post_id}/edit', [PostController::class,'edit']);
Route::put('/post/{post_id}',[PostController::class,'update']);
Route::delete('post/{post_id}',[PostController::class,'destroy']);

// Tugas CRUD
Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);
Route::get('/cast/{cast_id}',[CastController::class,'show']);
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{cast_id}',[CastController::class,'update']);
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);

// Uji Coba CRUD Eloquent 
Route::get('/film',[FilmController::class,'index']);
Route::get('/film/create',[FilmController::class,'create']);
Route::post('/film',[FilmController::class,'store']);
Route::get('/film/{film_id}',[FilmController::class,'show']);
Route::get('/film/{film_id}/edit',[FilmController::class,'edit']);
Route::put('/film/{film_id}',[FilmController::class,'update']);
Route::delete('/film/{film_id}',[FilmController::class,'destroy']);


// Tugas Template
Route::get('/', function(){
    return view('halaman.dashboard');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});
